﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_42.Controllers
{
    public class CodingIDController : Controller
    {
        // GET: CodingID
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.message = "Test";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.message = "Test";
            return View();
        }
        public ActionResult CodingIDMessage()
        {
            ViewBag.message = "Learn, Code, Share";
            return View();
        }
    }
}